FROM maven:3-jdk-8-alpine
VOLUME /tmp
EXPOSE 3001
RUN mkdir -p /app/
RUN mkdir -p /app/logs

#RUN sudo apt-get update && sudo apt-get install entr -yRUN mvn clean package --batch-mode

COPY pom.xml /app/
COPY src /app/src/
WORKDIR /app


#RUN mvn validate compile package
ADD target/*.jar /app/app.jar
#Depends on Image used. For alpine not provide bash shell instead of sh
#RUN bash -c 'touch app.jar'
#RUN sh 'touch app.jar'
ENTRYPOINT ["java","-jar","app.jar"]
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "app.jar"]