package com.example.app.result.voting.model;
import java.io.Serializable;

public class ResultVote implements Serializable {

    public ResultVote() {

    }

    private String percentCat;
    private String percentDog;
    private int totalVotes;


    public String getPercentCat() {
        return this.percentCat;
    }

    public void setPercentCat(String percentCat) {
        this.percentCat = percentCat;
    }

    public String getPercentDog() {
        return this.percentDog;
    }

    public void setPercentDog(String percentDog) {
        this.percentDog = percentDog;
    }

    public int getTotalVotes() {
        return this.totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

}