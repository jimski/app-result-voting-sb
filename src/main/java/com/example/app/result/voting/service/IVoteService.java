package com.example.app.result.voting.service;

import com.example.app.result.voting.model.Vote;

import java.util.List;

public interface IVoteService {
    public Vote findById(String theLang);
    public List<Vote> findAll();
}
