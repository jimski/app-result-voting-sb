package com.example.app.result.voting.service;

import com.example.app.result.voting.dao.IVoteDAO;
import com.example.app.result.voting.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
@Service
public class VoteServiceImpl implements IVoteService {

    private IVoteDAO voteDAO;

    @Autowired
    public VoteServiceImpl(IVoteDAO ipdpadao)
    {
        voteDAO=ipdpadao;
    }

    @Override
    @Transactional
    public Vote findById(String theId) {
        return voteDAO.findById(theId);
    }

    @Transactional
    public List<Vote> findAll() {
        return voteDAO.findAll();

    }
}
