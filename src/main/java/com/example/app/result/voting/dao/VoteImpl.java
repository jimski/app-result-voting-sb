package com.example.app.result.voting.dao;

import com.example.app.result.voting.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;
import org.hibernate.query.Query;


import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class VoteImpl implements IVoteDAO {

    @Autowired
    private EntityManager entityManager;


    //Setup Constructor injection
    @Autowired
    public VoteImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public VoteImpl() {
    }

    @Override
    public Vote findById(String theLang) {
        //get Current Hibernate Session
        Session currentSession = entityManager.unwrap(Session.class);
        //Get PDPA
        Vote thePdpaContent = currentSession.get(Vote.class,theLang);
        //Return result
        return thePdpaContent;
    }

    @Override
    public List<Vote> findAll() {
        //get Current Hibernate Session
        Session currentSession = entityManager.unwrap(Session.class);
        //Create Query
        Query<Vote> theQuery = currentSession.createQuery("from Vote",Vote.class);
        List<Vote> voteContent = theQuery.getResultList();
        return voteContent;
    }

}
